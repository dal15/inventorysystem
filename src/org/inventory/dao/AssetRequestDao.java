package org.inventory.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.inventory.dao.util.DbConnectionUtil;
import org.inventory.jdo.AssetRequest;

public class AssetRequestDao {

	
	public static List<AssetRequest> getAssetRequests()
	{
		List<AssetRequest> retRequests = new ArrayList<AssetRequest>();
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query = "SELECT REQUEST_ID, REQUEST_FOR, DURATION, USER_NAME ";
		query += " FROM ASSET_REQUESTS WHERE STATUS = 0";
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
		
			rs = pst.executeQuery();
			
			while(rs.next())
			{
				AssetRequest assReq = new AssetRequest();
				assReq.setRequestId(rs.getInt(1));
				assReq.setRequestFor(rs.getString(2));
				assReq.setDuration(rs.getString(3));
				assReq.setUserName(rs.getString(4));
				
				retRequests.add(assReq);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return retRequests;
	}
	
	public static AssetRequest getAssetRequest(String assetReqId)
	{
		AssetRequest retRequest = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query = "SELECT REQUEST_ID, REQUEST_FOR, DURATION, USER_NAME ";
		query += " FROM ASSET_REQUESTS WHERE REQUEST_ID = "+assetReqId;
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
		
			rs = pst.executeQuery();
			
			while(rs.next())
			{
				retRequest = new AssetRequest();
				retRequest.setRequestId(rs.getInt(1));
				retRequest.setRequestFor(rs.getString(2));
				retRequest.setDuration(rs.getString(3));
				retRequest.setUserName(rs.getString(4));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
			} catch (SQLException e1) {
			}
			
			try {
				con.close();
			} catch (SQLException e) {
			}
		}
		return retRequest;
	}
	
	public static boolean saveAssetRequest(String requestFor, String duration, 
		String userName)
	{
		boolean status = false;
		Connection con = null;
		PreparedStatement pst = null;
		String query = "INSERT INTO ASSET_REQUESTS (REQUEST_FOR, DURATION, USER_NAME, ";
			query += "STATUS ) VALUES (?,?,?,?)";
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
			pst.setString(1, requestFor);
			pst.setString(2, duration);
			pst.setString(3, userName);
			pst.setInt(4, 0);
			
			int rows = pst.executeUpdate();
			if(rows > 0)
			{
				status = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return status;
	}
	
	public static boolean updateAssetRequest(int requestId, int status)
	{
		boolean updateStatus = false;
		Connection con = null;
		PreparedStatement pst = null;
		String query = "UPDATE ASSET_REQUESTS SET STATUS = ? WHERE REQUEST_ID = ?";
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
			pst.setInt(1, status);
			pst.setInt(2, requestId);
			
			int rows = pst.executeUpdate();
			if(rows > 0)
			{
				updateStatus = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
			} catch (SQLException e1) {
			}
			
			try {
				con.close();
			} catch (SQLException e) {
			}
		}
		return updateStatus;
	}
	
	/*
	
	public static boolean updateCourseDetail(String course, String sp, String fa, int sections)
	{
		boolean status = false;
		Connection con = null;
		PreparedStatement pst = null;
		String query = "UPDATE COURSES SET SP=?, FA=?, SECTIONS=? WHERE COURSE = ?";
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
			pst.setString(1, sp);
			pst.setString(2, fa);
			pst.setInt(3, sections);
			pst.setString(4, course);
			
			int rows = pst.executeUpdate();
			if(rows > 0)
			{
				status = true;
				for(int i = 1; i <= sections;i++)
				{
					Section sec = getSection(course, i);
					if(sec == null)
					{
						Section secNew = new Section();
						secNew.setCourse(course);
						secNew.setSection(i);
						secNew.setMondayFlag(1);
						secNew.setTuesdayFlag(0);
						secNew.setWednesdayFlag(1);
						secNew.setThursdayFlag(0);
						secNew.setFridayFlag(1);
						
						saveSection(secNew);
					}
				}
				deleteSections(course, sections);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try {
				pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return status;
	}
	
	public static List<Section> getSections(String course)
	{
		List<Section> retList = new ArrayList<Section>();
		
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String courseTable = course.replaceAll(" ", "_");
		String query = "SELECT ID, COURSE, SECTION_NO, MONDAY_FLAG, TUESDAY_FLAG, "+
			"WEDNESDAY_FLAG, THURSDAY_FLAG, FRIDAY_FLAG, START_TIME, END_TIME FROM "+
			courseTable+" WHERE COURSE = ?";
		
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
			pst.setString(1, course);
			rs = pst.executeQuery();
			while(rs.next())
			{
				Section sec = new Section();
				sec.setId(rs.getInt(1));
				sec.setCourse(rs.getString(2));
				sec.setSection(rs.getInt(3));
				sec.setMondayFlag(rs.getInt(4));
				sec.setTuesdayFlag(rs.getInt(5));
				sec.setWednesdayFlag(rs.getInt(6));
				sec.setThursdayFlag(rs.getInt(7));
				sec.setFridayFlag(rs.getInt(8));
				sec.setStartTime(rs.getString(9));
				sec.setEndTime(rs.getString(10));
				
				System.out.println("sat = "+sec.getFridayFlag());
				
				retList.add(sec);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try {
				pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return retList;
	}
	
	public static Section getSection(String course, int section)
	{
		Section sec = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String courseTable = course.replaceAll(" ", "_");
		String query = "SELECT ID, COURSE, SECTION_NO, MONDAY_FLAG, TUESDAY_FLAG, "+
			"WEDNESDAY_FLAG, THURSDAY_FLAG, FRIDAY_FLAG, START_TIME, END_TIME FROM "+courseTable+
			" WHERE COURSE = ? AND SECTION_NO = ?";
		
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
			pst.setString(1, course);
			pst.setInt(2, section);
			rs = pst.executeQuery();
			while(rs.next())
			{
				sec = new Section();
				sec.setId(rs.getInt(1));
				sec.setCourse(rs.getString(2));
				sec.setSection(rs.getInt(3));
				sec.setMondayFlag(rs.getInt(4));
				sec.setTuesdayFlag(rs.getInt(5));
				sec.setWednesdayFlag(rs.getInt(6));
				sec.setThursdayFlag(rs.getInt(7));
				sec.setFridayFlag(rs.getInt(8));
				sec.setStartTime(rs.getString(9));
				sec.setEndTime(rs.getString(10));

				System.out.println("fri = "+sec.getFridayFlag());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try {
				pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return sec;
	}
	
	public static boolean saveSection(Section sec)
	{
		boolean status = false;
		Connection con = null;
		Statement st = null;
		PreparedStatement pst = null;
		String courseTable = sec.getCourse().replaceAll(" ", "_");
		String query = "INSERT INTO "+courseTable+" (COURSE, SECTION_NO, "+
			"MONDAY_FLAG, TUESDAY_FLAG, WEDNESDAY_FLAG, THURSDAY_FLAG, FRIDAY_FLAG)"+
			" VALUES (?,?,?,?,?,?,?)";
		try {
			con = DbConnectionUtil.getConnection();
			st = con.createStatement();
			String tableQuery = "CREATE TABLE "+courseTable
				+ "("
				+ "	ID INTEGER(10) AUTO_INCREMENT PRIMARY KEY,"
				+ "	COURSE VARCHAR(200),"
				+ "	SECTION_NO INTEGER(10),"
				+ "	START_TIME VARCHAR(20),"
				+ "	END_TIME VARCHAR(20),"
				+ "	MONDAY_FLAG INTEGER(1),"
				+ "	TUESDAY_FLAG INTEGER(1),"
				+ "	WEDNESDAY_FLAG INTEGER(1),"
				+ "	THURSDAY_FLAG INTEGER(1),"
				+ "	FRIDAY_FLAG INTEGER(1)"
				+ ");";
			try {
				st.execute(tableQuery);
			} catch (Exception e) {
			}
			
			pst = con.prepareStatement(query);
			pst.setString(1, sec.getCourse());
			pst.setInt(2, sec.getSection());
			pst.setInt(3, sec.getMondayFlag());
			pst.setInt(4, sec.getTuesdayFlag());
			pst.setInt(5, sec.getWednesdayFlag());
			pst.setInt(6, sec.getThursdayFlag());
			pst.setInt(7, sec.getFridayFlag());
			
			
			int rows = pst.executeUpdate();
			if(rows > 0)
			{
				status = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
			} catch (SQLException e1) {
			}
			
			try {
				con.close();
			} catch (SQLException e) {
			}
		}
		return status;
	}
	
	public static boolean updateSection(Section sec)
	{
		boolean status = false;
		Connection con = null;
		PreparedStatement pst = null;
		String courseTable = sec.getCourse().replaceAll(" ", "_");
		String query = "UPDATE "+courseTable+" SET MONDAY_FLAG = ?, "
			+ " TUESDAY_FLAG = ?, WEDNESDAY_FLAG = ?, THURSDAY_FLAG = ?, FRIDAY_FLAG = ?, "
			+ " START_TIME = ?, END_TIME = ? "
			+ " WHERE COURSE = ? AND SECTION_NO = ?";
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
			pst.setInt(1, sec.getMondayFlag());
			pst.setInt(2, sec.getTuesdayFlag());
			pst.setInt(3, sec.getWednesdayFlag());
			pst.setInt(4, sec.getThursdayFlag());
			pst.setInt(5, sec.getFridayFlag());
			pst.setString(6, sec.getStartTime());
			pst.setString(7, sec.getEndTime());
			pst.setString(8, sec.getCourse());
			pst.setInt(9, sec.getSection());
			
			int rows = pst.executeUpdate();
			if(rows > 0)
			{
				status = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return status;
	}
	
	public static boolean deleteSections(String course, int section)
	{
		boolean status = false;
		Connection con = null;
		PreparedStatement pst = null;
		String courseTable = course.replaceAll(" ", "_");
		String query = "DELETE FROM "+courseTable+" WHERE COURSE = ? AND SECTION_NO > ?";
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
			pst.setString(1, course);
			pst.setInt(2, section);
			
			int rows = pst.executeUpdate();
			if(rows > 0)
			{
				status = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return status;
	}
	*/
}
