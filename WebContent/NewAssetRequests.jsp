<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*, org.inventory.dao.*, org.inventory.jdo.*" %>

<html>
<jsp:include page="Header.jsp">
   	<jsp:param value="test" name="test"/>
</jsp:include>
<body class="bg-steel">
	<div style="width:85%; margin:0 auto;">
    <jsp:include page="Menus.jsp">
    	<jsp:param value="test" name="test"/>
	</jsp:include>
    <div class="page-content">
        <div class="flex-grid no-responsive-future" style="height: 100%;">

		<div class="row"> <!-- style="height: 100%" -->
		    <div class="cell auto-size padding20 bg-white" id="cell-content">
		        <h3 class="text-light"> New Asset Requests </h3>
		        <hr class="thin bg-grayLighter">
	        <table class="dataTable border bordered" data-auto-width="false" style="width:80%;">
	        	<thead>
	        		<tr>
	        		<th style="width: 40%;"> Request For </th>
	        		<th style="width: 20%;"> Duration </th>
	        		<th style="width: 20%;"> User Name </th>
	        		<th> Action </th>
	        		</tr>
	        	</thead>
	            <tbody>
<%
	String action = request.getParameter("action");
	System.out.println("action = "+action);
	if(action == null)
	{
		List<AssetRequest> assetReqs = AssetRequestDao.getAssetRequests();
		
		for(AssetRequest aReq : assetReqs)
		{
%>
	            <tr>
	                <td>
	                	<%=aReq.getRequestFor() %>
	 				</td>
	                <td style="height: 20px;">
	                 	<%=aReq.getDuration() %>
		  			</td>
		  			<td style="height: 20px;">
	                 	<%=aReq.getUserName() %>
		  			</td>
		  			<td>
	                 	<a href="./AssignAsset.jsp?assetreqid=<%=aReq.getRequestId()%>">Assign Asset</a>
		  			</td>
	             </tr>
<%
		}
%>
				</tbody>
		         </table>
		    </div>
		</div>
<%
	}
	else
	{
%>
		<div class="row"> <!-- style="height: 100%" -->
		    <div class="cell auto-size padding20 bg-white" id="cell-content">
		        <h3 class="text-light">
<%
		//System.out.println("action = "+action);
		String request_for = request.getParameter("request_for");
		String duration = request.getParameter("duration");
		String userName = (String)session.getAttribute("username");
		
		boolean status = AssetRequestDao.saveAssetRequest(request_for, 
			duration, userName);
		if(status)
		{
			out.println("Request submitted successfully.");
		}
		else
		{
			out.println("Error submitting Request;"); 
			out.println("Please contact System Admin or try again later.");
		}
%>
				</h3>
		        <hr class="thin bg-grayLighter">
		    </div>
		</div>
<%
	}
%>
        </div>
    </div>
    </div>
</body>
</html>